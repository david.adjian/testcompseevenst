package com.example.testcompose

import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.material.Checkbox
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.testcompose.data.TestBetModel


//class ImageModelPreviewParameterProvider : PreviewParameterProvider<ImageModel> {
//    override val values: Sequence<ImageModel>
//        get() = sequence {
//            ImageModel(
//                id = 0,
//                mainImageUrl = "",
//                title = "Text",
//                image1Url = "https://api.lorem.space/image/face?w=150&h=150&v=",
//                image2Url =  "https://api.lorem.space/image/face?w=150&h=150&v=",
//                image1BackgroundUrl = "",
//                image2BackgroundUrl = "", user1Kef = 0.0f, user2Kef = 0.0f,
//            )
//        }
//
//}

//@Preview
@Composable
fun ListItem(
//    @PreviewParameter(ImageModelPreviewParameterProvider::class)
    testBetModel: TestBetModel
) {

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .height(200.dp)
    ) {

//        AsyncImage(
//            model = testBetModel.mainImageUrl,
//            contentDescription = null,
//            contentScale = ContentScale.Crop,
//            placeholder = painterResource(id = R.drawable.main_placeholder)
//        )

        Image(
            modifier = Modifier.fillMaxWidth(),
            contentScale = ContentScale.Crop,
            painter = painterResource(id = R.drawable.main_placeholder),
            contentDescription = ""
        )

        Row(
            modifier = Modifier
                .fillMaxWidth()
        ) {
            Box(
                Modifier
                    .fillMaxWidth()
                    .weight(1f)
            ) {


                Text(
                    modifier = Modifier
                        .background(Color.White)
                        .padding(top = 0.dp),
                    text = testBetModel.user1Kef.toString(),
                    fontSize = 14.sp,
                    color = Color.Black,
                )
//                AsyncImage(
//                    model = testBetModel.image1Url,
//                    contentDescription = null,
//                    modifier = Modifier
//                        .padding(top = 80.dp)
//                        .align(Alignment.Center)
//                        .width(100.dp)
//                        .height(100.dp)
//                        .border(3.dp, Color.Green),
//                    contentScale = ContentScale.Crop,
//                    placeholder = painterResource(id = R.drawable.user_placeholder)
//                )
                val image1 = remember {
                    if (testBetModel.btn1Kef) {
                        R.drawable.user_placeholder
                    } else {
                        R.drawable.face2
                    }
                }

                Image(
                    contentDescription = null,
                    modifier = Modifier
                        .padding(top = 80.dp)
                        .align(Alignment.Center)
                        .width(100.dp)
                        .height(100.dp)
                        .border(3.dp, Color.Green),
                    contentScale = ContentScale.Crop,
                    painter = painterResource(id = image1)
                )

                var _btn1Kef by remember {
                mutableStateOf(testBetModel.btn1Kef)
            }
                val animatedAlpha1 by animateFloatAsState(
                    targetValue = if (_btn1Kef) 1.0f else 0f,
                    label = "alpha"
                )
                Box(
                    modifier = Modifier
                        .padding(top = 70.dp)
                        .graphicsLayer {
                            alpha = animatedAlpha1
                        }
                        .align(Alignment.Center)
                        .width(120.dp)
                        .height(120.dp)
                        .border(3.dp, Color.Green),
                ) {

                }
                if (_btn1Kef) {
                    Image(
                        contentDescription = null,
                        modifier = Modifier
                            .padding(top = 80.dp)
                            .align(Alignment.Center)
                            .width(100.dp)
                            .height(100.dp)
                            .border(3.dp, Color.Green),
                        contentScale = ContentScale.Crop,
                        painter = painterResource(id = R.drawable.face2)
                    )
                } else {
                    Image(
                        contentDescription = null,
                        modifier = Modifier
                            .padding(top = 80.dp)
                            .align(Alignment.Center)
                            .width(100.dp)
                            .height(100.dp)
                            .border(3.dp, Color.Green),
                        contentScale = ContentScale.Crop,
                        painter = painterResource(id = R.drawable.user_placeholder)
                    )
                }
                Checkbox(
                    checked = testBetModel.btn1Kef,
                    onCheckedChange = {
                        _btn1Kef = !testBetModel.btn1Kef
                        testBetModel.btn1Kef = _btn1Kef
                    },
                    modifier = Modifier
                        .padding(top = 5.dp)
                        .align(Alignment.TopCenter)
                )
            }
            Box(
                Modifier
                    .fillMaxWidth()
                    .weight(1f)
            ) {
                Text(
                    text = testBetModel.title,
                    modifier = Modifier
                        .background(color = Color(0x55000000))
                        .fillMaxWidth()
                        .align(Alignment.Center),
                    fontSize = 20.sp
                )
            }
            Box(
                Modifier
                    .fillMaxWidth()
                    .weight(1f)
            ) {

//                AsyncImage(
//                    model = testBetModel.image2BackgroundUrl,
//                    contentDescription = null,
//                    modifier = Modifier
//                        .padding(top = 20.dp)
//                        .align(Alignment.Center)
//                        .width(150.dp)
//                        .height(150.dp),
//                    contentScale = ContentScale.Crop,
//                    alpha = 0.8f
//                )
                Text(
                    modifier = Modifier
                        .background(Color.White)
                        .padding(top = 0.dp),
                    text = testBetModel.user2Kef.toString(),
                    fontSize = 14.sp,
                    color = Color.Black
                )
//                AsyncImage(
//                    model = testBetModel.image2Url,
//                    contentDescription = null,
//                    modifier = Modifier
//                        .padding(top = 80.dp)
//                        .align(Alignment.Center)
//                        .width(100.dp)
//                        .height(100.dp)
//                        .border(3.dp, Color.Green),
//                    contentScale = ContentScale.Crop,
//                    placeholder = painterResource(id = R.drawable.user_placeholder)
//                )

                var _btn2Kef by remember {
                    mutableStateOf(testBetModel.btn2Kef)
                }
                val animatedAlpha by animateFloatAsState(
                    targetValue = if (_btn2Kef) 1.0f else 0f,
                    label = "alpha"
                )
                Box(
                    modifier = Modifier
                        .padding(top = 70.dp)
                        .graphicsLayer {
                            alpha = animatedAlpha
                        }
                        .align(Alignment.Center)
                        .width(120.dp)
                        .height(120.dp)
                        .border(3.dp, Color.Green),
                ) {

                }
                if (_btn2Kef) {
                    Image(
                        contentDescription = null,
                        modifier = Modifier
                            .padding(top = 80.dp)
                            .align(Alignment.Center)
                            .width(100.dp)
                            .height(100.dp)
                            .border(3.dp, Color.Green),
                        contentScale = ContentScale.Crop,
                        painter = painterResource(id = R.drawable.user_placeholder)
                    )
                } else {
                    Image(
                        contentDescription = null,
                        modifier = Modifier
                            .padding(top = 80.dp)
                            .align(Alignment.Center)
                            .width(100.dp)
                            .height(100.dp)
                            .border(3.dp, Color.Green),
                        contentScale = ContentScale.Crop,
                        painter = painterResource(id = R.drawable.face2)
                    )
                }


                Checkbox(
                    checked = testBetModel.btn2Kef,
                    onCheckedChange = {
                        _btn2Kef = !testBetModel.btn2Kef
                        testBetModel.btn2Kef = _btn2Kef
                    },
                    modifier = Modifier
                        .padding(top = 5.dp)
                        .align(Alignment.TopCenter)
                )
            }

        }
    }


}

@Preview
@Composable
fun ListItemPreview() {

    ListItem(TestBetModel(
        id = 9317,
        title = "ne",
        image1Url = "http://www.bing.com/search?q=appetere",
        image2Url = "https://duckduckgo.com/?q=eleifend",
        image1BackgroundUrl = "https://search.yahoo.com/search?p=habeo",
        image2BackgroundUrl = "https://search.yahoo.com/search?p=libero",
    ).apply {
        mainImageUrl = "https://www.google.com/#q=mea"
        user1Kef = "duo"
        user2Kef = "class"
    }
    )

}

@Composable
fun Greeting3(name: Int) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .background(Color.Red)
    ) {
        Text(
            text = "Hello $name!",
            modifier = Modifier
                .padding(18.dp),
            color = Color.White,
        )
        LazyRow {

        }
    }

}


@Composable
fun Greeting(name: Int) {
    Text(
        text = "Hello $name!",
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp)
    )
}


@Composable
fun Greeting2(name: Int) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .background(Color.Cyan)
    ) {
        Text(
            text = "Hello $name!",
            modifier = Modifier
                .padding(18.dp),
            color = Color.Black,
        )
    }
}

@Composable
fun Greeting4(name: Int) {
    Column(
        modifier = Modifier
            .padding(8.dp)
            .background(Color.Yellow),
    ) {
        Text(
            text = "# $name!",
            modifier = Modifier
                .padding(28.dp),
            color = Color.Black,
        )

    }
}

