package com.example.testcompose

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.example.testcompose.data.TestBetModel
import com.example.testcompose.ui.theme.TestComposeTheme
import kotlinx.coroutines.flow.MutableStateFlow

class MainActivity : ComponentActivity() {

    val itemFlow = MutableStateFlow(listOf<TestBetModel>())

    // private val itemList = mutableStateListOf<ImageModel>()

    val viewModel: MyViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            MainScreen()
        }
    }

    var array1: MutableList<Int>? = null
    var array2: MutableList<Int>? = null


    fun onClick() {
//        viewModel.onClick1(array1)
//        viewModel.onClick1(array1)
    }

    fun onClick2() {
//        startActivity(intent)
        //  viewModel.onClick2(array2)
    }


    @Composable
    fun MainScreen(viewModel: MyViewModel = this.viewModel) {
        val uiState by viewModel.uiState.collectAsState()

        TestComposeTheme {
            // A surface container using the 'background' color from the theme
            Surface(
                modifier = Modifier.fillMaxSize(),
            ) {
                Column(
                    modifier = Modifier.fillMaxWidth()
                ) {
                    Row {
                        Text(
                            text = "Button 1",
                            modifier = Modifier
                                .clickable(
                                    onClick = ::onClick,
                                    indication = rememberRipple(bounded = true),
                                    interactionSource = remember { MutableInteractionSource() },
                                )
                                .padding(10.dp),
                        )
                        Text(
                            text = "Button 2",
                            modifier = Modifier
                                .clickable(
                                    onClick = ::onClick2,
                                    indication = rememberRipple(bounded = true),
                                    interactionSource = remember { MutableInteractionSource() },
                                )
                                .padding(10.dp),
                        )
                        CircularProgressIndicator()
                    }


                    LazyColumn {
                        items(
                            items = uiState.itemList,
                            key = {
                                it.id
                            }
                        ) {
                            ListItem(it)
                        }
                    }
                }
            }
        }
    }

}


