package com.example.testcompose.data

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue

data class TestBetModel(
    val id: Int,
    val title: String,
    val image1Url: String,
    val image2Url: String,
    val image1BackgroundUrl: String,
    val image2BackgroundUrl: String,
) {
    var mainImageUrl: String by mutableStateOf("")
    var user1Kef: String by mutableStateOf("")
    var user2Kef: String by mutableStateOf("")
    var btn1Kef: Boolean by mutableStateOf(false)
    var btn2Kef: Boolean by mutableStateOf(false)
}