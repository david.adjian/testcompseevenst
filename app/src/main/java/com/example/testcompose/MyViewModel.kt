package com.example.testcompose

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.testcompose.data.TestBetModel
import com.example.testcompose.repo.DataGenerator
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlin.random.Random

data class UiState(
    val itemList: List<TestBetModel> = listOf(),
)

class MyViewModel : ViewModel() {


    private val _uiState = MutableStateFlow(UiState())
    val uiState: StateFlow<UiState> = _uiState.asStateFlow()


//    fun onClick1(array: MutableList<Int>?) {
//        array?.let {
//            val a = sortArray(it)
//            data.postValue(a)
//        }
//    }

    init {

        DataGenerator()
            .getDataFlow()
            .onEach {
                _uiState.value = _uiState.value.copy(itemList = it)
            }
            .flowOn(Dispatchers.IO)
            .launchIn(viewModelScope)
    }

    fun generateArray(): MutableList<Int> {
        val listA = mutableListOf<Int>()
        for (i in 0..5_000_000) {
            listA.add(Random.nextInt())
            if (i >= 1_000_000 && i % 1_000_000 == 0) {
                Log.e("ACASC", "progress $i")
            }
        }

        return listA
    }

    private val viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)


//    fun onClick2(array: MutableList<Int>?) {
//        viewModelScope.launch(Dispatchers.Default) {
////            Thread.sleep(5000)
//            array?.let {
//                val a = sortArray(it)
//                data.postValue(a)
//            }
//        }
//    }

    fun sortArray(array: MutableList<Int>): MutableList<Int> {
        Log.e("ACASC", "coroutineScope.launch Thread  ${Thread.currentThread().name}")
        val timeStart = System.currentTimeMillis()
        Log.e("ACASC", "time start $timeStart")

        Log.e("ACASC", "start create sort")
        array.sort()
        val timeEnd = System.currentTimeMillis()
        Log.e("ACASC", "time end $timeEnd")
        Log.e("ACASC", "time to sort  ${timeEnd - timeStart}")
        return array
    }
}