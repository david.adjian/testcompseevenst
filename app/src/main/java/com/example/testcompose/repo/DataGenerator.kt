package com.example.testcompose.repo

import android.util.Log
import com.example.testcompose.data.TestBetModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class DataGenerator {
    private val itemList = mutableListOf<TestBetModel>()

    init {
        for (i in 1..500) {
            val model = TestBetModel(
                i,
                "Битва №$i",
                "https://random.imagecdn.app/150/150?w=150&h=150&v=${i}",
                "https://random.imagecdn.app/150/150?w=150&h=150&v=${-i}",
                "https://random.imagecdn.app/150/150?w=150&h=150&v=${i}",
                "https://random.imagecdn.app/150/150?w=150&h=150&v=${-i}",
            ).apply {
                mainImageUrl = "https://random.imagecdn.app/300/150?category=house"
                user1Kef = "${i * 10.5424f}"
                user2Kef = "${i * 80.284f}"
            }
            itemList.add(model)
        }
    }

    fun getDataFlow(): Flow<List<TestBetModel>> {
        return flow {
            var x = 1
            while (true) {
                delay(200)
                for (i in 0 until itemList.size) {
                    x++
                    if (i % 2 == 0) {
                        itemList[i].apply {
                            user1Kef = "${itemList[i].user1Kef.toDouble() * 1.51}"
                            user2Kef = "${itemList[i].user1Kef.toDouble() + 1}"
                        }
//                        if (x%5 == 0) {
//                            itemList[i].apply {
//                                mainImageUrl = "https://random.imagecdn.app/150/150?v=$x"
//                            }
//                        }
                    }
                }
                Log.e( "getDataFlow", "$x")
                emit(itemList)
            }
        }
    }

}